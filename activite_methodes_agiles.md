![Scrum Board](https://img-0.journaldunet.com/WCttPN_jrkJtFQt4A6ZBJ532bGs=/1280x/smart/32fa42e37bff4c2db1af8b8d38f5f97a/ccmcms-jdn/15286988.jpg)

# Activité : Découverte des méthodes Agiles

> On parle **DES** méthodes agiles !

Les méthodes Agiles (on parle également _d'Agilité_) ont envahies le dictionnaire
des développeurs et chefs de projets depuis ces 20 dernières années!

On parle bien **des** méthodes Agiles car elles représentent un éventail de méthodes
répondant à différents besoins et types d'organisation de projet.

Nous allons aujourd'hui nous concentrer sur la méthode la plus utilisées lors du
développement de projet numériques : **SCRUM**.

Mais sachez également qu'il existe les méthodes **Kanban**, **eXtreme Programming**, etc.

## Partie 1

Par groupe de 3/4 apprenants, vous devez rechercher la signification des termes suivants
(si vous en trouvez d'autre, c'est encore mieux) :

- Product Owner
- Scrum Master
- Epic (Epopée)
- Sprint
- User Story
- Tâches
- Sprint Planning
- Daily Stand-up
- Scrum Review
- Sprint Retrospective
- Scrum Board
- Kanban
- User Story Mapping
- Backlog Produit
- Story Point
- Poker Planning
- Burndown Chart
- Peer Programming
- Test-Driven Development (TDD)

Tentez de porter vos recherches sur l'utilité de chaque concept dans le contexte de la
gestion de projet numérique en équipe.

## Partie 2

Avec les définitions des termes d'agilité trouvées dans la première partie,
essayer d'identifier la **correspondance de chaque mot-clé** avec les concepts
utilisés pour la gestion du 2e projet (tâches, livrables, étapes, itérations, tableau, planning, etc.).

## Partie 3

Afin de mieux comprendre comment tous les concepts clés de l'Agilité sont connectés entre eux, tenter de les **classer en 3 ou 4 grandes catégories**,
afin d'obtenir un début de "cartographie" des termes de l'Agilité.

## Jeux Scrum

[Scrum Card Game Online](https://scrumcardgame.com/online/)

## Documentation

[Scrum Guides (anglais)](https://scrumguides.org/)

[Memo Scrum (français)](Memo_Scrum_V1.2.pdf)

## Méthodologie Scrum

[Coding Machine - Scrum](https://www.thecodingmachine.com/methodologie-scrum/)

![Coding Machine - Scrum](https://www.thecodingmachine.com/wp-content/uploads/2019/01/Scrum-par-thecodingmahcine.jpg)

[My Agile Partner - Les Ceremonies Scrum](https://blog.myagilepartner.fr/index.php/2018/08/17/ceremonies-sprint-scrum/)

![My Agile Partner - Les Ceremonies Scrum](https://blog.myagilepartner.fr/wp-content/uploads/2017/01/Untitled-Diagram.png)

[Editions ENI - Cycle de Vie Scrum](https://www.editions-eni.fr/open/mediabook.aspx?idR=b48d25ea277899fa9b04164796f9b7fc)

![Editions ENI - Cycle de Vie Scrum](https://www.editions-eni.fr/Open/download/a1f31608-4192-4625-92b8-7bfeb75b8af6/images/Picture4.PNG)

[Neon Rain - Agile Scrum Framework](https://neonrain.com/agile-scrum-web-development/)

![Neon Rain - Agile Scrum Framework](https://neonrain.com/wp-content/uploads/2017/02/scrum_process_afa_5000.jpg)

## Theme vs Epic vs Story vs Task

![Theme vs Epic vs Story vs Task](https://i.pinimg.com/originals/5b/52/66/5b5266945d16420a7c3415397598a4e2.jpg)

## Scrum Board

![Exemple Scrum Board](https://usefyi.com/wp-content/uploads/2019/09/Scrum-Board.png)

## User Story Mapping

![Exemple User Story Mapping 1](https://powerslides.com/wp-content/uploads/2019/02/User-Story-Mapping-3.jpg)

![Exemple User Story Mapping 2](https://i.pinimg.com/originals/70/21/78/70217838f098355d978c3841719332a6.png)

## Burndown Chart

[Scrum League - Burndown Chart](https://scrum-league.org/tribune/autres-notions-scrum/le-burndown-chart/)

[Nutcache - Burndown Chart](https://www.nutcache.com/fr/blog/scrum-burndown-chart/)

[Scrum Institute - Burndown Chart](https://www.scrum-institute.org/Burndown_Chart.php)

![Exemple Burndown Chart 1](https://cdn.elearningindustry.com/wp-content/uploads/2017/11/1a4f191a96141df4ee9aebd6475999f6.png)

![Exemple Burndown Chart 2](https://help.wrike.com/hc/user_images/wZll2tv7A1xaNx8YkIAegQ.png)

![Exemple Burndown Chart 3](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3YlzpKbXio1G4KbOVQdpIX7XsZzfoFmPvjQ&usqp=CAU)
