# Activité : Découverte de la gestion projet

## Partie 1 (1h)

Par groupes de différentes tailles :

- 1 : François B.
- 2 : Mérouane, Xavier
- 3 : Maxime, Helena, Cinthya
- 4 : Malika, Hamida, Arnaud, Gabriel
- 7 : Moncef, Thienvu, Théo, Anthony, Kamel, François O., Rayane

Recopier sur une feuille papier A4 le "Zen Of Python":

```python
import this
```

Contraintes :

- Tout le monde doit participer
- Trouver la stratégie la plus adapté au travail en groupe
- Chronométrer le temps total (mesurer séparément le temps de répartition et de travail)
- Le but étant de mettre le moins de temps possible pour remplir l'objectif

### Observations

Points évoqués :

- Définir le fil conducteur / la préparation / la vue d'ensemble
- Se répartir les tâches
- Parallèliser le travail si possible
- Définir le rôle de chacun : répartition, écriture, contrôle
- Effectuer des vérifications locales et globales
- Essayer d'estimer le temps de chaque tâches
- Prendre le temps de la mise en commun
- Coordonner les membre et les tâches d'une équipe

![Zen Of Python](img/activite-decouverte-zen-python.jpg)

## Partie 2 (2h)

Reprendre projet 1 en binome et :

- Lister les objectifs / livrables attendus
- Lister toutes les tâches qui étaient nécessaires pour atteindre chaque objectif
- Pour chaque tâche, essayer d'estimer le temps de travail nécessaire basé sur l'expérience du projet

### Observations

2 approches possibles pour plannifier un projet :

- Par grandes étapes du projet
    1. Définir les grandes étapes (ex : extraction, transformation, chargement, exploitation, visualisation)
    2. Définir les tâches nécessaires pour chaque étape
    3. Se répartir les tâches d'une même étape et avancer ensemble sur chaque étape

- Par objectifs / livrables successifs
    1. Définir chaque objectif / livrable attendu au fur et à mesure du projet
    2. Pour chaque objectif, définir le minimum de tâches nécessaire pour le réaliser
    3. Se répartir les tâches de chaque objectif
    4. Chaque objectif va couvrir un peu de chacunes des grandes étapes du projet

**Nous allons privilégier l'approche dite "incrémentale" par objectifs pour les projets futurs.**

![Approche Par Objectifs](img/activite-decouverte-approche-objectifs.jpg)
